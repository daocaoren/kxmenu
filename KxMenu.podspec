
Pod::Spec.new do |s|
  s.name         = "KxMenu"
  s.version      = "0.0.1"
  s.summary      = "KxMenu."
  s.description  = "KxMenu just for us"
  s.homepage     = "http://www.pacer.cc"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.platform     = :ios, "7.0"
  s.author       = { "Ray" => "guolei@me.com" }
  s.source       = {
    :git => 'https://daocaoren@bitbucket.org/daocaoren/kxmenu.git',
    :tag => '0.0.1'
  }
  s.frameworks  = "UIKit"
  s.requires_arc = true
end
